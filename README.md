# The Daily Zevenarist

The Daily Zevenarist is free software application intended for those who want easy Zevenarist Religious Teachings at their fingertips. It is licensed under the MIT license, which means you are free to use it in any way that makes sense for your purposes. You can even make money from this if you wish!

You could also donate a few dollars to help support the development of this app and keep it up-to-date.
We could always use some helping hands as well, If you know the basics of HTML and CSS, you could help us out by helping edit and format the pages!
Thank you for taking the time to read this, and we hope you find our app useful.

## Learn More

[Website](https://zevenar.neocities.org/index.html)

Social Media
- [Ruqqus](https://ruqqus.com/+classic4chan)
- [Lemmy](https://lemmy.ml/c/zevenarism)
